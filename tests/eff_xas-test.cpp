#include <gtest/gtest.h>
#include <iostream>
#include <armadillo>
#include "../src/eff_xas.cpp"

// print debug info
bool debug = false;
bool light_debug = false;

TEST(eff_xas, run) {
    std::string output_xas = "../../examples/xas/h2s_xas_soc_vdz.out";
    eff_xas test_eff_xas(output_xas);
    test_eff_xas.run();
    EXPECT_EQ(16, test_eff_xas.get_states().size());
    EXPECT_FLOAT_EQ(167.556, test_eff_xas.get_lowest_energy());
}