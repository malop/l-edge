#include <gtest/gtest.h>
#include "../src/utils.cpp"

std::string output_xas = "../../examples/xas/h2s_xas_soc_vdz.out";
std::string output_xps = "../../examples/xps/h2s_xps_soc_vdz.out";
std::string output_ip = "../../examples/ip/h2s_ip_soc_vdz.out";

TEST(utils, check_output) {
    ASSERT_EQ(1, check_output(output_xas));
    ASSERT_EQ(2, check_output(output_xps));
    ASSERT_EQ(3, check_output(output_ip));
}
