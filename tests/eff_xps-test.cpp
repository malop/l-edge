#include <gtest/gtest.h>
#include <iostream>
#include <armadillo>
#include "../src/eff_xps.cpp"

// print debug info
bool debug = false;
bool light_debug = false;

TEST(eff_xps, run) {
    std::string output_xps = "../../examples/xps/h2s_xps_soc_vdz.out";
    eff_xps test_eff_xps(output_xps);
    test_eff_xps.run();
    EXPECT_EQ(3, test_eff_xps.get_states().size());
    EXPECT_FLOAT_EQ(172.7466, test_eff_xps.get_lowest_energy());
    EXPECT_EQ(2, test_eff_xps.get_states()[0].get_mult());
    EXPECT_NEAR(-2702.86, test_eff_xps.get_eigenval()[0], 1e-2);
    EXPECT_NEAR(-1881.16, test_eff_xps.get_eigenval()[2], 1e-2);
    EXPECT_NEAR(6932.69, test_eff_xps.get_eigenval()[4], 1e-2);

}

TEST(eff_xps, ip) {
    std::string output_ip = "../../examples/ip/h2s_ip_soc_vdz.out";
    eff_xps test_eff_xps(output_ip);
    test_eff_xps.run(false);
    EXPECT_EQ(3, test_eff_xps.get_states().size());
    EXPECT_FLOAT_EQ(172.7466, test_eff_xps.get_lowest_energy());
    EXPECT_EQ(2, test_eff_xps.get_states()[0].get_mult());
    EXPECT_NEAR(-2702.86, test_eff_xps.get_eigenval()[0], 1e-2);
    EXPECT_NEAR(-1881.16, test_eff_xps.get_eigenval()[2], 1e-2);
    EXPECT_NEAR(6932.69, test_eff_xps.get_eigenval()[4], 1e-2);
}