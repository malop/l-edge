#ifndef STATE_H
#define STATE_H

#include <iostream>
#include <string>
#include <vector>
#include "utils.h"

extern bool debug;
extern bool light_debug;

class State {
protected:                                  
    std::string m_state; // name
    size_t m_mult;       // multiplicity 
    double m_energy = 0; // energy

public: 
    State(std::string state, size_t mult) : m_state(state), m_mult(mult) {
        if (debug)
            std::cout << "A new state " << m_state << " has been found." << std::endl;
    };
    State(std::string state,
          double energy,
          size_t mult)
        : m_state(state), m_energy(energy), m_mult(mult) {
        if (debug)
            std::cout << "A new state " << m_state << " has been found."
                      << " with energy: " << m_energy << " eV. "
                      << std::endl;
    };
    
    void set_energy(double energy) { m_energy = energy;};
    
    double get_energy_in_eV() const { return m_energy;};
    double get_energy_in_cm() const { return m_energy*ev2cm;};
    std::string get_state() const { return m_state;};
    size_t get_mult() const { return m_mult;};
};

#endif // STATE_H
