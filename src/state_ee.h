#ifndef STATE_EE_H
#define STATE_EE_H

#include "state.h"
#include <iostream>
#include <string>
#include <vector>
extern bool debug;
extern bool light_debug;

class State_ee : public State {
private:                                  
    double m_osc_str = 0;        // oscillator strength
    std::vector<double> m_tdm1;  // transition density matrix 1->2
    std::vector<double> m_tdm2;  // transition density matrix 2->1

public: 
    State_ee(std::string state,
             double energy, 
             size_t mult)
        : State(state, energy, mult)
    {

        if (debug)
            std::cout << "A new state " << state << " has been found"
                 << " with energy: " << energy << " eV. " 
                 << std::endl;
    };

    void add_tdm1(double tdm_i) { m_tdm1.push_back(tdm_i);};
    void add_tdm2(double tdm_i) { m_tdm2.push_back(tdm_i);};
    void set_osc_str(double osc_str) { m_osc_str = osc_str;};

    double get_osc_str() const { return m_osc_str;};
    std::vector<double> get_tdm1() const { return m_tdm1;};
    std::vector<double> get_tdm2() const { return m_tdm2;};

};

class Singlet : public State_ee {
private:
public:
    Singlet(std::string state, double energy) 
    : State_ee(state, energy, 1) {
        if (debug) std::cout << "It is a singlet!"  << std::endl;
    };
};

class Triplet : public State_ee {
private:
public:
    Triplet(std::string state, double energy) 
    : State_ee(state, energy, 3) {
        if (debug)
            std::cout << "It is a triplet!" << std::endl;
    };
};

#endif // STATE_EE_H
