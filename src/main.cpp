#include <chrono>
#include <ctime>

#include "utils.h"
#include "eff_xps.h"
#include "eff_xas.h"

// print debug info
bool debug = false;
bool light_debug = false;

// main function
int main(int argc, char **argv) {
    if (argc < 2) {
        printf("error: missing output file as argument.\n");
        return 1;
    }
    std::string output = argv[1];

    auto start = std::chrono::system_clock::now();
    std::time_t start_time = std::chrono::system_clock::to_time_t(start);

    std::cout << "Starting at " << std::ctime(&start_time) << std::endl;

    
    // check output and run:
    switch (check_output(output)) {
    case 1: eff_xas(output).run();
        break;
    case 2: eff_xps(output).run();
        break;
    case 3: eff_xps(output).run(false);
        break;
    default:
        std::cout << "Invalid output" << std::endl;
        break;
    }

    auto end = std::chrono::system_clock::now();

    std::chrono::duration<double> elapsed_seconds = end - start;
    std::time_t end_time = std::chrono::system_clock::to_time_t(end);

    
    std::cout << "Finished at " << std::ctime(&end_time)
              << "Elapsed time: " << elapsed_seconds.count() << "s\n";

    return 0;
}
