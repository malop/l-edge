#include "eff_xps.h"

void eff_xps::run(bool dyson) {
    if (debug) std::cout << "running eff_xps" << std::endl;

    std::cout << "Finding the non-relativistic states..." << std::endl << std::endl;
    find_states(); // find NR states

    if (dyson) find_dyson(); // find dyson coefficients

    std::cout << "Building the SO Hamiltonian..." << std::endl << std::endl;
    build_ham(); // build hamiltonian

    std::cout << "Diagonalizing the SO Hamiltonian..." << std::endl << std::endl;
    diag_ham(); // diagonalize hamiltonian

    if (dyson) build_dyson_matrix(); // build dyson matrix to get intensities
    if (dyson) Dyson_orbitals(); // transform Dyson orbitals
    
    // print results
    std::cout << "Printing results..." << std::endl << std::endl;
    print_nr_results(dyson);
    print_so_results(dyson);
    print_results();

}

// get lowest energy - the energies in the diagonal are shifted wrt the lowest energy
double eff_xps::get_lowest_energy() {
    double shift = 10000000;
    for (size_t i = 0; i < m_states.size(); ++i)
    {
        if (m_states[i].get_energy_in_eV() < shift) {
            shift = m_states[i].get_energy_in_eV();
        }
    }
    return shift;
}


// find the NR states
void eff_xps::find_states() {
    std::ifstream inFile;
    inFile.open(m_output);
    std::string line;
    while (getline(inFile, line)) {
        std::string pattern_transition("EOMIP transition");
        std::string pattern_energy("Excitation energy");
        if (line.find(pattern_transition) != std::string::npos) {
            std::vector<std::string> dummy_line;
            std::istringstream iss(line);
            copy(std::istream_iterator<std::string>(iss),std::istream_iterator<std::string>(),back_inserter(dummy_line));
            State_ip new_state(dummy_line[2]);
            m_states.push_back(new_state);
        }
        if (line.find(pattern_energy) != std::string::npos) {
            std::vector<std::string> dummy_line;
            std::istringstream iss(line);
            copy(std::istream_iterator<std::string>(iss),
                 std::istream_iterator<std::string>(),
                 back_inserter(dummy_line));
            m_states[m_states.size()-1].set_energy(stod(dummy_line[8]));
        }

    }
    inFile.close();
    if (debug) {
        for (const auto& s:m_states) {
            std::cout << "State: " << s.get_state()
                 << " has energy : " << s.get_energy_in_eV()
                 << " eV." << std::endl;
        }
    }
}

// find the Dyson orbital coefficients
void eff_xps::find_dyson() {
    std::ifstream inFile;
    inFile.open(m_output);
    std::string line;
    size_t nstate = 0;
    bool left_DO_over_AO = false;
    bool right_DO_over_AO = false;
    while (getline(inFile, line)) {
        std::string pattern_left_norm("Left Dyson orbital norm is ");
        std::string pattern_left_DO_over_AO("Decomposition over AOs for the left alpha Dyson orbital:");
        std::string pattern_right_norm("Right Dyson orbital norm is ");
        std::string pattern_right_DO_over_AO("Decomposition over AOs for the right alpha Dyson orbital:");
        std::string pattern_endline("*****");
        std::string pattern_empty("^$");
        if (line.find(pattern_left_norm) != std::string::npos) {
            std::vector<std::string> dummy_line;
            std::istringstream iss(line);
            copy(std::istream_iterator<std::string>(iss),std::istream_iterator<std::string>(),back_inserter(dummy_line));
            m_states[nstate].set_left_norm(stod(dummy_line[5]));
        }
        else if (line.find(pattern_left_DO_over_AO) != std::string::npos) {
            left_DO_over_AO = true;
        }
        else if (left_DO_over_AO) {
            if (line.find(pattern_endline) != std::string::npos) {
                left_DO_over_AO = false;
            } else {
                m_states[nstate].add_left_DO_over_AO(stod(line));
            }
        }  
        else if (line.find(pattern_right_norm) != std::string::npos) {
            std::vector<std::string> dummy_line;
            std::istringstream iss(line);
            copy(std::istream_iterator<std::string>(iss),std::istream_iterator<std::string>(),back_inserter(dummy_line));
            m_states[nstate].set_right_norm(stod(dummy_line[5]));
        }
        else if (line.find(pattern_right_DO_over_AO) != std::string::npos) {
            right_DO_over_AO = true;
        }
        else if (right_DO_over_AO) {
            if (line.find(pattern_endline) != std::string::npos) {
                right_DO_over_AO = false;
                ++nstate;
            } else {
                m_states[nstate].add_right_DO_over_AO(stod(line));
            }
        }      
    }
    inFile.close();  
    if (debug) {
        for (const auto& s:m_states)
        {
            std::cout << "State: " << s.get_state()
                 << " has energy: " << s.get_energy_in_eV()
                 << " eV, left Dyson norm: " << s.get_left_norm()
                 << " and right norm: " << s.get_right_norm() 
                 << "." << std::endl;
        }
    }
}

// put block in matrix - used in read_H_blocks()
void eff_xps::put_block_in(const arma::Mat<std::complex<double>> &H_Block,
                           size_t offset_i,
                           size_t offset_j) {
    for(size_t i = 0; i < H_Block.n_rows; i++)
        for(size_t j = 0; j < H_Block.n_cols; j++) {
            m_ham(i+offset_i, j+offset_j) = H_Block(i, j);
            m_ham(j+offset_j, i+offset_i) = std::conj(H_Block(i, j));
        }
}

// fill the diagonal of the Hso matrix - with the NR excitation energies shifted wrt the lowest one
void eff_xps::fill_diagonal() {
    if (debug) std::cout << "fill_diagonal(): start" << std::endl;
    size_t ncount = 0;
    double shift = 1000000;
    shift = get_lowest_energy(); 

    for (size_t i = 0; i < m_states.size(); ++i) {
        m_ham(ncount,ncount) = (m_states[i].get_energy_in_eV()-shift)*ev2cm;
        m_ham(ncount+1,ncount+1) = (m_states[i].get_energy_in_eV()-shift)*ev2cm;
        ncount += 2;
    }
    if (debug) {
        std::cout << "The diagonal of the Hamiltonian is: \n";
        for (size_t i = 0; i < m_ham.n_cols; ++i) {
            std::cout << m_ham(i,i) << std::endl;
        }
        std::cout << std::endl;
    }
    if (debug) std::cout << "fill_diagonal(): end" << std::endl;
}

// read H blocks from the m_output
void eff_xps::read_H_blocks() {
    //Read m_output
    std::ifstream inFile;
    inFile.open(m_output);

    std::string line;

    size_t is1 = 0; //state1
    size_t is2 = is1+1; //state2
    size_t i_offset = 0; //row
    size_t j_offset = 0; //column
    size_t i_row = 0;
    size_t ncols = 2; //ket is a doublet
    size_t nline = 0;
    size_t nrows = 2; //bra is a doublet
    size_t n_pattern_matrix = 0;
    size_t n_matrix_rows = 0;
    bool found_state_A = false;
    bool found_state_B = false;
    bool found_actual_matrix = false;
    arma::Mat< std::complex<double> > subH(nrows,ncols);

    //Iterate through file
    while (std::getline(inFile, line)) {
        ++nline;
        if (is1 < m_states.size()) {
            if (is2 < m_states.size()) {
                std::string pattern_matrix ("Actual matrix element");
                std::string pattern_state_A ("State A: cvs_eomip_ccsd/a: "+m_states[is1].get_state());
                std::string pattern_state_B ("State B: cvs_eomip_ccsd/a: "+m_states[is2].get_state());
                if (line.find(pattern_state_A) != std::string::npos) {
                    found_state_A = true;
                    found_state_B = false;
                    if (debug) std::cout << "Found state A: " << m_states[is1].get_state() << " in line: " << nline << std::endl << line << std::endl;
                    j_offset = is1*m_mult; 
                } else if (found_state_A && (line.find(pattern_state_B) != std::string::npos)) {
                    if (debug) std::cout << "Found state B " << m_states[is2].get_state() << " in line: " << nline << std::endl << line << std::endl;
                    found_state_A = false;
                    found_state_B = true;
                    i_row = 0;
                    i_offset = is2*m_mult;
                } else if (found_state_B && line.find(pattern_matrix) != std::string::npos) {
                    ++n_pattern_matrix;
                } else if (n_pattern_matrix == 5) {
                    found_actual_matrix = true;
                } else if (found_actual_matrix) {
                    ++n_matrix_rows;
                    if (n_matrix_rows > 1 && n_matrix_rows <= 1+nrows) {
                        std::string word;
                        size_t j = 0;
                        while (line[j] != '(') {
                            ++j;
                        }
                        word = line.substr(j,line.length() -j);
                        j = 0;
                        std::istringstream iss(word);
                        for (size_t i_column = 0; i_column < ncols; ++i_column) {
                            iss >> subH(i_row,i_column); 
                        }
                        i_row++;
                    } else if (n_matrix_rows > 1+nrows) {
                        if (debug) {
                            std::cout << "subH.raw_print(std::cout)" << std::endl;
                            subH.raw_print(std::cout);
                        }
                        put_block_in(subH, i_offset, j_offset);
                        //Reset values
                        found_actual_matrix = false;
                        n_matrix_rows = 0;
                        n_pattern_matrix = 0;
                        ++is2;
                        if (is2 == m_states.size()) {
                            ++is1;
                            is2 = is1+1;
                        }
                    }
                }
            }
        }
    }
}

// construct the SO Hamiltonian (Hso)
void eff_xps::build_ham() {
    m_size = m_mult * m_states.size();                        //Total number of states
    m_ham.set_size(m_size,m_size);
    m_ham.zeros();
    //diagonal
    fill_diagonal(); //fill diagonal with excitation energies differences
    //off-diagonal
    read_H_blocks(); //fill off-diagonal with Hso matrix elements
    
}

// diagonalize the SO Hamiltonian 
void eff_xps::diag_ham() {
    //Diagonalize Hamiltonian:
    arma::eig_sym(m_eigval, m_eigvec, m_ham);

    if (debug) {
        std::cout << "The eigenvalues of H are (in cm-1)" << std::endl;
        m_eigval.raw_print(std::cout);
        std::cout << std::endl;
        std::cout << "The eigenvectors" << std::endl;
        m_eigvec.raw_print(std::cout);
    }
}

// construct DO matrix
void eff_xps::build_dyson_matrix() {
    m_dys_mat.set_size(m_size, m_size);
    m_dys_mat.zeros();

    if (debug)
    {
        std::cout << "This is the Dyson matrix" << std::endl;
        m_dys_mat.raw_print(std::cout);
        std::cout << std::endl;
    }

    size_t ni = 0;
    size_t nj = 0;
    if (debug) std::cout << "m_states.size() = " << m_states.size() << std::endl;
    for (size_t i = 0; i < m_dys_mat.n_rows; ++i) {
        arma::vec L = m_states[ni].get_left_DO_over_AO();
        for (size_t j = 0; j < m_dys_mat.n_cols; ++j) {
            arma::vec R = m_states[nj].get_right_DO_over_AO();
            m_dys_mat(i,j) = dot(L,R);
            if (j % 2 != 0) ++nj;
        }
        if (i % 2 != 0) ++ni;
        nj = 0;
    }
    m_dys_trans = m_eigvec.t() * m_dys_mat * m_eigvec;
}

// print results
void eff_xps::print_results() {
    std::cout << "This is the compiled SO Hamiltonian (cm-1)" << std::endl;
    m_ham.raw_print(std::cout);
    std::cout << std::endl;

    std::cout << "The eigenvalues of H are (in cm-1)" << std::endl;
    m_eigval.raw_print(std::cout);
    std::cout << std::endl;
    
    if (debug) {
        std::cout << "The diagonal of the m_dys_trans is: \n";
        for (size_t i = 0; i < m_dys_trans.n_cols; ++i)
        {
            std::cout << m_dys_trans(i, i).real() << std::endl;
        }
        std::cout << std::endl;
    }

    if (debug) {
        std::cout << "eigvec: \n";
        m_eigvec.raw_print(std::cout);
    }
}

// print NR results to file
void eff_xps::print_nr_results(bool dyson)
{
    std::ofstream outFile;
    std::string filename = m_output.substr(0,m_output.size()-4)+"_NR.txt";
    outFile.open(filename);
    for (size_t i = 0; i < m_states.size(); ++i) {
        outFile << std::setprecision(4) << std::setw(8) << std::fixed << m_states[i].get_energy_in_eV();
        if (dyson) outFile << std::setprecision(6) << std::setw(12) << std::fixed << m_states[i].get_left_norm() * m_states[i].get_right_norm(); 
        outFile<< std::endl;
    }    
    outFile.close();
    std::cout << "The non-relativistic results have been written to: " << filename << std::endl << std::endl;
}

// print SO results to file
void eff_xps::print_so_results(bool dyson)
{
    std::ofstream outFile;
    std::string filename = m_output.substr(0,m_output.size()-4)+"_SO.txt";
    outFile.open(filename);
    double shift = get_lowest_energy();
    for (size_t i = 0; i < m_eigval.size(); ++i) {
        double energy = m_eigval[i]/ev2cm+shift;
        if (!dyson) {
            if (i %2 == 0) outFile << std::setprecision(4) << std::setw(8) << std::fixed << energy << std::endl;
        } else {
            outFile << std::setprecision(4) << std::setw(8) << std::fixed << energy;
            outFile << std::setprecision(6) << std::setw(12) << std::fixed << m_dys_trans(i, i).real();
            outFile << std::endl;
        }
    }
    outFile.close();
    std::cout << "The spin-corrected results results have been written to: " << filename << std::endl << std::endl;
}

//This function transforms the DO coefficients decomposed over the AO basis to plot the transform DOs
void eff_xps::Dyson_orbitals()
{
    
    int fact = 1;
    size_t AO = m_states[0].get_left_DO_over_AO().size();
    size_t nstate = 0;
    arma::Mat<double> DO_AO_RIGHT(AO,m_size);
    arma::Mat<double> DO_AO_LEFT(m_size,AO);

    for (size_t i = 0; i < m_size; ++i)
    {
        for (size_t j = 0; j < AO; ++j)
        {
            DO_AO_RIGHT(j,i) = fact*m_states[nstate].get_right_DO_over_AO()[j];
            DO_AO_LEFT(i,j) = fact*m_states[nstate].get_left_DO_over_AO()[j];
        }

        if (i % 2 != 0) 
        {
            if (debug) {
            std::cout << "for nstate = " << nstate <<", fact = " << fact;
            for (auto c : m_states[nstate].get_left_DO_over_AO() )
            {
                std::cout << fact*c << std::endl;
            }
            }

            ++nstate;
            fact = 1; 
        } else {
            if (debug)
            {
            std::cout << "for nstate = " << nstate <<", fact = " << fact;
            for (auto c : m_states[nstate].get_left_DO_over_AO() )
            {
                std::cout << fact*c << std::endl;
            }
            }
            fact = -1;
        }  
    }
    if (debug)
    {
        std::cout << "This is the DO_AO_RIGHT: " << std::endl;
        DO_AO_RIGHT.raw_print(std::cout);
    }
    if (debug)
    {
        std::cout << "This is the DO_AO_LEFT: " << std::endl;
        DO_AO_LEFT.raw_print(std::cout);
    }

    arma::cx_mat DO_AO_RIGHT_TRANS = DO_AO_RIGHT * m_eigvec;
    arma::cx_mat DO_AO_LEFT_TRANS = m_eigvec.t()*DO_AO_LEFT;
    
    arma::cx_mat UtU = m_eigvec.t()*m_eigvec;
    arma::cx_mat UUt = m_eigvec*m_eigvec.t();
    if (debug)
    {
        std::cout << "This is the DO_AO_RIGHT_TRANS: " << std::endl;
        DO_AO_RIGHT_TRANS.raw_print(std::cout);
    }
    if (debug)
    {
        std::cout << "This is the DO_AO_LEFT_TRANS: " << std::endl;
        DO_AO_LEFT_TRANS.raw_print(std::cout);
    }
    if (debug)
    {
        std::cout << "This is U: " << std::endl;
        m_eigvec.raw_print(std::cout);

    }

    std::ofstream outFile;
    std::ofstream outFile2;
    std::string filename = m_output.substr(0,m_output.size()-4)+"_left_do_so.mo";
    std::string filename2 = m_output.substr(0,m_output.size()-4)+"_right_do_so.mo";
    outFile.open(filename);
    outFile2.open(filename2);
    m_ham_trans = m_eigvec.t() * m_ham * m_eigvec;

    for (size_t i = 0; i < m_size; ++i)
    {
            outFile << "Sym=X\nEne="<< m_ham_trans(i,i).real() << "\nSpin=Alpha\nOccup=1\n";
            outFile2 << "Sym=X\nEne="<< m_ham_trans(i,i).real() << "\nSpin=Alpha\nOccup=1\n";

        for (size_t j = 0; j < AO; ++j)
        {
                    outFile
                    << j+1 << "\t" 
                        << DO_AO_LEFT_TRANS(i,j)
                 << std::endl;
                    outFile2 
                    << j+1 << "\t" 
                        << DO_AO_RIGHT_TRANS(j,i) 
                 << std::endl;
        }
    }


    outFile.close();
    outFile2.close();
}

