#include "eff_xas.h"

void eff_xas::run() {
    if (debug) std::cout << "running" << std::endl;

    // Read symmetry - no need for this
    // std::string point_group;
    // std::vector<std::string> irreps;
    // read_sym(point_group, irreps);

    std::cout << "Finding the non-relativistic states..." << std::endl << std::endl;
    find_states();
    std::cout << "Building the SO Hamiltonian..." << std::endl << std::endl;
    build_ham();
    std::cout << "Diagonalizing the SO Hamiltonian..." << std::endl << std::endl;
    diag_ham();
    std::cout << "Transforming the transition density matrices..." << std::endl << std::endl;
    tdm();
    print_nr_results();
    print_so_results();

};

// get lowest energy - the energies in the diagonal are shifted wrt the lowest energy
double eff_xas::get_lowest_energy() {
    double shift = 10000000;
    for (size_t i = 0; i < m_states.size(); ++i) {
        if (m_states[i].get_energy_in_eV() < shift) {
            shift = m_states[i].get_energy_in_eV();
        }
    }
    return shift;
}


// read the symmetry of the molecule
void eff_xas::read_sym(std::string &point_group, 
              std::vector<std::string> &irreps){
    //Read m_output
    std::ifstream inFile;
    inFile.open(m_output);
    //Iterate through file
    std::string line;
    int nline = 0;
    while (getline(inFile, line)) {
        ++nline;
        //Find if point group and irreps
        std::string pattern_point_group("Point group: ");
        if (line.find(pattern_point_group) != std::string::npos) {
            point_group = line.substr(14, 3);
            ++nline;
        }
        if (nline == 1) {
            nline = 0;
        } else if (nline == 4) {
            std::istringstream iss(line);
            copy(std::istream_iterator<std::string>(iss),
                 std::istream_iterator<std::string>(),
                 back_inserter(irreps));
            irreps.pop_back();
        }
    }
    inFile.close();
    if (debug) {
        std::cout << "The point group symmetry is: " << point_group << std::endl;
        std::cout << "with irreducible representation(s): " << std::endl;
        for(const auto& irrep : irreps) {
            std::cout << irrep << std::endl;   
        }
        std::cout << std::endl;   
    }
}

// find the NR states
void eff_xas::find_states() {
    //Declaration of variables
    bool found_state_A = false;
    bool found_state_B = false;
    size_t tdm = 0;
    size_t istate = 0;
    //Read m_output
    std::ifstream inFile;
    //Iterate through file
    inFile.open(m_output);
    std::string line;
    while (getline(inFile, line) && (!inFile.eof())) {
        std::string pattern_ref_state ("State A: ccsd: 0/");
        std::string pattern_singlet ("State B: cvs_eomee_ccsd/rhfref/singlets:");
        std::string pattern_triplet ("State B: cvs_eomsf_ccsd:");
        std::string pattern_energy ("Energy GAP");
        std::string pattern_tdm ("Transition dipole moment");
        std::string pattern_os ("Oscillator strength");
        if (line.find(pattern_ref_state) != std::string::npos) {
            found_state_A = true;
            found_state_B = false;
        } else if (found_state_A && ((line.find(pattern_singlet) != std::string::npos))) {
            if (debug) std::cout << line << std::endl;
            found_state_A = false;
            found_state_B = true;
            std::istringstream iss(line);
            std::string word;
            for (size_t i = 0; i < 4; i++) iss >> word;
            std::string sym = word;
            
            getline(inFile, line);
            std::istringstream iss2(line);
            for (size_t i = 0; i < 7; i++) iss2 >> word;
            double energy = std::stod(word);

            Singlet dummy_state(sym,energy);
            m_states.push_back(dummy_state);
            ++istate;
            m_singlets.push_back(dummy_state);
        } else if (found_state_A && ((line.find(pattern_triplet) != std::string::npos))) {
            if (debug) std::cout << line << std::endl;
            found_state_A = false;
            found_state_B = true;
            std::istringstream iss(line);
            std::string word;
            for (size_t i = 0; i < 4; i++) iss >> word;
            std::string sym = word;
            getline(inFile, line);
           
            std::istringstream iss2(line);
            for (size_t i = 0; i < 7; i++) iss2 >> word;
            double energy = std::stod(word);

            Triplet dummy_state(sym,energy);
            m_states.push_back(dummy_state);
            ++istate;
            m_triplets.push_back(dummy_state);
        } else if (found_state_B && ((line.find(pattern_tdm) != std::string::npos))) {
            if (debug) std::cout << "found tdm pattern!" << std::endl;
            ++tdm;
        } else if (tdm == 1 ) {
            ++tdm;
            std::istringstream iss(line);
            std::vector<std::string> dummy_line;
            copy(std::istream_iterator<std::string>(iss),
                 std::istream_iterator<std::string>(),
                 back_inserter(dummy_line));
            for (size_t i = 0; i < 3; ++i) {
                if (debug) {
                    std::cout << "(dummy_line[3+2*i]): " << (dummy_line[3+2*i]) << std::endl;
                    std::cout << (dummy_line[3+2*i]).substr(0,8) << std::endl;
                    std::cout << istate << std::endl;
                }
                m_states[istate-1].add_tdm1((std::stod((dummy_line[3+2*i]).substr(0,8))));
            }
        } else if (tdm == 2 ) {
            ++tdm;
            std::istringstream iss(line);
            std::vector<std::string> dummy_line;
            copy(std::istream_iterator<std::string>(iss),
                 std::istream_iterator<std::string>(),
                 back_inserter(dummy_line));
            for (size_t i = 0; i < 3; ++i) {
                if (debug) {
                    std::cout << "(dummy_line[3+2*i]): " << (dummy_line[3+2*i]) << std::endl;
                    std::cout << (dummy_line[3+2*i]).substr(0,8) << std::endl;
                    std::cout << istate << std::endl;
                }
                m_states[istate-1].add_tdm2(std::stod((dummy_line[3+2*i]).substr(0,8)));
            }
        } else if (found_state_B && ((line.find(pattern_os) != std::string::npos))) {
            tdm = 0;
            found_state_B = false;
           std::stringstream iss(line);
            std::vector<std::string> dummy_line;
            copy(std::istream_iterator<std::string>(iss),
                 std::istream_iterator<std::string>(),
                 back_inserter(dummy_line));
            m_states[istate-1].set_osc_str(std::stod((dummy_line[3])));
            if (debug) {
                    std::cout << "Oscillator strength" << std::endl;
                    std::cout << "(dummy_line[3]): " << (dummy_line[3]) << std::endl;
                    std::cout << std::endl;
                }

        }
    }
    inFile.close();
    if (light_debug) {
        std::cout << "Zero states:" << std::endl;
        std::cout << "states.size() = " << m_states.size() << std::endl;
        for (size_t i = 0; i < m_states.size(); ++i) {
            if (m_states[i].get_mult() == 1) {
                std::cout << "Singlet "; 
            } else if (m_states[i].get_mult() == 3) {
                std::cout << "Triplet ";
            }
            std::cout << m_states[i].get_state()
                 << "   E = " <<  m_states[i].get_energy_in_eV()
                 << "   f = " <<  m_states[i].get_osc_str() << std::endl
                 << "   TDM GS-ES:  X " << m_states[i].get_tdm1()[0] 
                 << "   Y " << m_states[i].get_tdm1()[1]
                 << "   Z " << m_states[i].get_tdm1()[2]
                 << std::endl
                 << "   TDM ES-GS:  X " << m_states[i].get_tdm2()[0] 
                 << "   Y " << m_states[i].get_tdm2()[1]
                 << "   Z " << m_states[i].get_tdm2()[2];
                 std::cout << std::endl << std::endl;
        }
    }
}



// put block in matrix - used in read_H_blocks()
void eff_xas::put_block_in(const arma::Mat<std::complex<double> > &H_Block, 
                           size_t offset_i, 
                           size_t offset_j) {
    for(size_t i = 0; i < H_Block.n_rows; i++)
        for(size_t j = 0; j < H_Block.n_cols; j++) {
            m_ham(i+offset_i, j+offset_j) = H_Block(i, j);
            m_ham(j+offset_j, i+offset_i) = std::conj(H_Block(i, j));
        }
}

// fill the diagonal of the matrix Hso - with the NR excitation energies shifted wrt the lowest one 
void eff_xas::fill_diagonal() {
    if (debug) std::cout << "fill_diagonal() start" << std::endl;
    size_t ncount = 0;
    double shift = 1000000;
    shift = get_lowest_energy(); 

    for (size_t i = 0; i < m_states.size(); ++i) {
        if (debug) std::cout << "i = " << i << std::endl;
        if (debug) std::cout << "ncount = " << ncount << std::endl;
        
        if (m_states[i].get_mult() == 1) {
            m_ham(ncount,ncount) = (m_states[i].get_energy_in_eV()-shift)*ev2cm;
            ++ncount;
        } else if (m_states[i].get_mult() == 3) {
            m_ham(ncount,ncount) = (m_states[i].get_energy_in_eV()-shift)*ev2cm;
            m_ham(ncount+1,ncount+1) = (m_states[i].get_energy_in_eV()-shift)*ev2cm;
            m_ham(ncount+2,ncount+2) = (m_states[i].get_energy_in_eV()-shift)*ev2cm;
            ncount = ncount + 3;
                
        }
    }
    if (debug) {
        std::cout << "The diagonal of the Hamiltonian is: \n";
        for (size_t i = 0; i < m_ham.n_cols; ++i) {
            std::cout << m_ham(i,i) << std::endl;
        }
        std::cout << std::endl;
    }
    if (debug) std::cout << "fill_diagonal() end" << std::endl;
}

// read H blocks from the m_output
void eff_xas::read_H_blocks() {
    //Read m_output
    std::ifstream inFile;
    inFile.open(m_output);

    std::string line;
    size_t nline = 0;
    size_t i_offset = 0; //row
    size_t j_offset = 0; //column
    size_t is = 0; //singlet
    size_t it = 0; //triplet
    size_t it1 = m_triplets.size(); //triplet1
    size_t it2 = m_triplets.size(); //triplet2
    size_t n_pattern_matrix = 0;
    size_t n_matrix_rows = 0;
    size_t i_row = 0;
    size_t nrows = 3; //bra is a triplet
    size_t ncols = 1; //ket is a singlet
    bool found_state_A = false;
    bool found_state_B = false;
    bool found_actual_matrix = false;
    arma::Mat< std::complex<double> > subH(nrows,ncols);

    //Iterate through file
    while (std::getline(inFile, line)) {
        ++nline;
        //if (debug) std::cout << "LINE Nº: " << nline << std::endl << line << std::endl;

    //singlets-triplets
        if (is < m_singlets.size()) {
            if (it < m_triplets.size()) {
                std::string pattern_matrix ("Actual matrix elements:");
                std::string pattern_state_A ("State A: cvs_eomee_ccsd/rhfref/singlets: "+m_singlets[is].get_state()+"");
                std::string pattern_state_B ("State B: cvs_eomsf_ccsd: "+m_triplets[it].get_state()+"");
                if (line.find(pattern_state_A) != std::string::npos) {
                    found_state_A = true;
                    if (debug) std::cout << "Found state A: " << m_singlets[is].get_state() << " in line: " << nline << std::endl << line << std::endl;
                    j_offset = is; 
                } else if (found_state_A && line.find(pattern_state_B) != std::string::npos) {
                    if (debug) std::cout << "Found state B " << m_triplets[it].get_state() << " in line: " << nline << std::endl << line << std::endl;
                    found_state_B = true;
                    i_row = 0;
                    i_offset = m_singlets.size() + it*3;
                } else if (found_state_B && line.find(pattern_matrix) != std::string::npos) {
                    if (debug) std::cout << "Found pattern matrix" << std::endl;
                    if (debug) std::cout << "n_pattern_matrix = " << n_pattern_matrix << std::endl;
                    ++n_pattern_matrix;
                } else if (n_pattern_matrix == 5) {
                    found_actual_matrix = true;
                } else if (found_actual_matrix) {
                    ++n_matrix_rows;
                    if (n_matrix_rows > 1 && n_matrix_rows < 5) {
                       std::string word;
                        size_t j = 0;
                        while (line[j] != '(') {
                            ++j;
                        }
                        word = line.substr(j,line.length() -j);
                        j = 0;
                        std::istringstream iss(word);
                        for (size_t i_column = 0; i_column < ncols; ++i_column) {
                            iss >> subH(i_row,i_column); 
                        }
                        i_row++;
                    } else if (n_matrix_rows == 5) {
                        if (debug) {
                            std::cout << "subH.raw_print(cout)" << std::endl;
                            subH.raw_print(std::cout);
                        }
                        put_block_in(subH, i_offset, j_offset);
                        //Reset values
                        found_actual_matrix = false;
                        n_matrix_rows = 0;
                        n_pattern_matrix = 0;
                        found_state_A = false;
                        found_state_B = false;
                        ++it;
                        if (debug) std::cout << "it = " << it << " and m_triplets.size() = " << m_triplets.size() << std::endl;
                        if (it == m_triplets.size()) {
                            ++is;
                            it = 0;
                        }
                    }
                }
            }
        // if singlets-triplets, done reinitialize parameters fro triplet-triplet
        } else if (is == m_singlets.size()) {
            {
                if (debug) std::cout << "is == singlets.size()"<< std::endl;
                it1 = 0; //triplet1
                it2 = 1; //triplet2
                i_row = 0;
                ncols = 3; //ket is a triplet
                subH.resize(nrows,ncols);
                ++is;
            }
        //triplets-triplets
        } else if (it1 < m_triplets.size()) {
            // if (debug) std::cout << "it1 < m_triplets.size()"<< std::endl;
            if (it2 < m_triplets.size()) {
                // if (debug) std::cout << "it2 < m_triplets.size()"<< std::endl;
                std::string pattern_matrix ("Actual matrix elements:");
                std::string pattern_state_A ("State A: cvs_eomsf_ccsd: "+m_triplets[it1].get_state()+"");
                std::string pattern_state_B ("State B: cvs_eomsf_ccsd: "+m_triplets[it2].get_state()+"");
                if (line.find(pattern_state_A) != std::string::npos) {
                    found_state_A = true;
                    found_state_B = false;
                    if (debug) std::cout << "Found state A: " << m_triplets[it1].get_state() << " in line: " << nline << std::endl << line << std::endl;
                    j_offset = m_singlets.size() + it1*3; 
                } else if (found_state_A && line.find(pattern_state_B) != std::string::npos) {
                    if (debug) std::cout << "Found state B " << m_triplets[it2].get_state() << " in line: " << nline << std::endl << line << std::endl;
                    found_state_A = false;
                    found_state_B = true;
                    i_row = 0;
                    i_offset = m_singlets.size() + it2*3;
                } else if (found_state_B && line.find(pattern_matrix) != std::string::npos) {
                    ++n_pattern_matrix;
                } else if (n_pattern_matrix == 5) {
                    found_actual_matrix = true;
                } else if (found_actual_matrix) {
                    ++n_matrix_rows;
                    if (n_matrix_rows > 1 && n_matrix_rows < 5) {
                       std::string word;
                        size_t j = 0;
                        while (line[j] != '(') {
                            ++j;
                        }
                        word = line.substr(j,line.length() -j);
                        j = 0;
                        std::istringstream iss(word);
                        for (size_t i_column = 0; i_column < ncols; ++i_column) {
                            iss >> subH(i_row,i_column); 
                        }
                        i_row++;
                    } else if (n_matrix_rows == 5) {
                        if (debug) {
                            std::cout << "subH.raw_print(cout)" << std::endl;
                            subH.raw_print(std::cout);
                        }
                        put_block_in(subH, i_offset, j_offset);
                        //Reset values
                        found_actual_matrix = false;
                        n_matrix_rows = 0;
                        n_pattern_matrix = 0;
                        ++it2;
                        if (it2 == m_triplets.size()) {
                            ++it1;
                            it2 = it1+1;
                        }
                    }
                }
            }
        }
    }
}

// construct the SO Hamiltonian (Hso)
void eff_xas::build_ham() {
    m_size = 3*m_triplets.size() + m_singlets.size();                       //Total number of states
    m_ham.set_size(m_size,m_size);
    m_ham.zeros();

    //diagonal
    fill_diagonal();
    //off-diagonal
    read_H_blocks();


}

// diagonalize the SO Hamiltonian 
void eff_xas::diag_ham() {
    //Diagonalize Hamiltonian:
    arma::eig_sym(m_eigval, m_eigvec, m_ham);

    if (light_debug) {
        std::cout << "The eigenvalues of H are (in cm-1)" << std::endl;
        m_eigval.raw_print(std::cout);
        std::cout << std::endl;
    }
    if (debug) {
        std::cout << "The eigenvectors" << std::endl;
        m_eigvec.raw_print(std::cout);
    }
}


// fill TDM1 GS -> ES
void eff_xas::fill_tdm1(arma::Mat<double>& TDM1, 
                         size_t alpha) {
    size_t iN = 0;
    for (size_t i = 0; i < m_states.size(); ++i) {
        for (size_t j = 0; j < m_states[i].get_mult(); ++j) {
            TDM1(0,iN) = m_states[i].get_tdm1()[alpha]; 
            ++iN;
        }
    }
}

// fill TDM2 ES -> GS
void eff_xas::fill_tdm2(arma::Mat<double>& TDM2, 
               size_t alpha) {
    size_t iN = 0;
    for (size_t i = 0; i < m_states.size(); ++i) {
        for (size_t j = 0; j < m_states[i].get_mult(); ++j) {
            TDM2(iN,0) = m_states[i].get_tdm2()[alpha]; 
            ++iN;
        }
    }
}

// construct transition dipole moment matrices (TDM) between ground state (GS) and excited states (ES)
void eff_xas::tdm() {
    arma::Mat<double>  TDM1_X(1,m_size), TDM1_Y(1,m_size), TDM1_Z(1,m_size);
    arma::Mat<double>  TDM2_X(m_size,1), TDM2_Y(m_size,1), TDM2_Z(m_size,1);
    fill_tdm1(TDM1_X, 0);
    fill_tdm1(TDM1_Y, 1);
    fill_tdm1(TDM1_Z, 2);
    fill_tdm2(TDM2_X, 0);
    fill_tdm2(TDM2_Y, 1);
    fill_tdm2(TDM2_Z, 2);

    m_tdm_x_trans = m_eigvec.t() * TDM2_X*TDM1_X * m_eigvec;
    m_tdm_y_trans = m_eigvec.t() * TDM2_Y*TDM1_Y * m_eigvec;
    m_tdm_z_trans = m_eigvec.t() * TDM2_Z*TDM1_Z * m_eigvec;
}


// print NR results to file: name+_NR.txt 
void eff_xas::print_nr_results() {
    std::ofstream outFile;
    std::string filename = m_output.substr(0,m_output.size()-4)+"_NR.txt";
    outFile.open(filename);
    for (size_t i = 0; i < m_states.size(); ++i) {
        outFile << std::setprecision(4) << std::setw(8) << std::fixed << m_states[i].get_energy_in_eV() 
                << std::setprecision(6) << std::setw(12) << std::fixed << m_states[i].get_osc_str() << std::endl;

    }
    if (light_debug) {
        std::cout << "This is the compiled SO Hamiltonian (cm-1):" << std::endl << std::endl;
        m_ham.raw_print(std::cout); 
        std::cout << std::endl;
    }
    outFile.close();

    std::cout << "The non-relativistic results have been written to: " << filename << std::endl << std::endl;
}

// print SO results to file: name+_SO.txt 
void eff_xas::print_so_results() {
    double shift = get_lowest_energy();
    std::ofstream outFile;
    std::string filename = m_output.substr(0,m_output.size()-4)+"_SO.txt";
    outFile.open(filename);
    for (size_t i = 0; i < m_eigval.size(); ++i) {
        double energy = m_eigval[i]/ev2cm+shift;
        double osc_str = (2.0/3.0)*(energy/au2ev)*(m_tdm_x_trans(i,i).real()+m_tdm_y_trans(i,i).real()+m_tdm_z_trans(i,i).real());
        outFile << std::setprecision(4) << std::setw(8) << std::fixed << energy
                << std::setprecision(6) << std::setw(12) << std::fixed << osc_str 
                << std::endl;
    }
    outFile.close();

    std::cout << "The spin-corrected results results have been written to: " << filename << std::endl << std::endl;
}

