#ifndef STATE_IP_H
#define STATE_IP_H

#include "state.h"
#include <iostream>
#include <string>
#include <vector>

extern bool debug;
extern bool light_debug;

class State_ip : public State {
private:                                  
    double m_left_norm;                     // left norm of DO
    double m_right_norm;                    // right norm of DO
    std::vector<double> m_left_DO_over_AO;  // AO coefficients of left DO
    std::vector<double> m_right_DO_over_AO; // AO coefficients of right DO

public: 
    State_ip(std::string state) : State(state, 2) {
        if (debug)
            std::cout << "A new ionized state " << m_state << " has been found." << std::endl;
    };

    void set_left_norm(double left_norm) { m_left_norm = left_norm;};
    void set_right_norm(double right_norm) { m_right_norm = right_norm;};
    void add_left_DO_over_AO(double coefficient) { m_left_DO_over_AO.push_back(coefficient);};
    void add_right_DO_over_AO(double coefficient) { m_right_DO_over_AO.push_back(coefficient);};
    
    double get_left_norm() const { return m_left_norm;};
    double get_right_norm() const { return m_right_norm;};
    std::vector<double> get_left_DO_over_AO() const { return m_left_DO_over_AO;};
    std::vector<double> get_right_DO_over_AO() const{ return m_right_DO_over_AO;};

};

#endif // STATE_IP_H
