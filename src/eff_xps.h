#ifndef EFF_XPS_H
#define EFF_XPS_H

#ifndef ARMA_DONT_USE_WRAPPER
#define ARMA_DONT_USE_WRAPPER
#endif // ARMA_DONT_USE_WRAPPER

#ifndef ARMA_USE_LAPACK
#define ARMA_USE_LAPACK
#endif // ARMA_USE_LAPACK

#include <armadillo>

#include <iostream> // standard input-output stream (cout/cin)
#include <iomanip> // for setprecison()
#include <fstream> // input/output file stream 
#include <string>
#include <vector>
#include <regex> // regular expressions

#include "state_ip.h"
#include "utils.h"

class eff_xps {
private:
    std::string m_output;                         // q-chem output to be read
    std::vector<State_ip> m_states;               // vector with states
    size_t m_mult = 2;                            // multiplicity (IP -> 2)
    size_t m_size = 0;                            // size of matrix
    arma::Mat<std::complex<double>> m_ham;        // hamiltonian
    arma::Mat<std::complex<double>> m_ham_trans;  // transformed hamiltonian
    arma::Mat<std::complex<double>> m_dys_mat;    // Dyson matrix
    arma::Mat<std::complex<double>> m_dys_trans;  // transformed Dyson matrix
    arma::cx_mat m_eigvec;                        // eigenvectors
    arma::vec m_eigval;                           // eigenvalues

public:
    eff_xps(std::string output) : m_output(output) {};

    std::vector<State_ip> get_states() const {return m_states;}
    arma::vec get_eigenval() const {return m_eigval;}
    
    //functions:
    void run(bool dyson = true);

    // get lowest energy - the energies in the diagonal are shifted wrt the lowest energy
    double get_lowest_energy();

    // find the NR states
    void find_states();

    // find the Dyson orbital coefficients
    void find_dyson();


    // put block in matrix - used in read_H_blocks()
    void put_block_in(const arma::Mat<std::complex<double>> &H_Block,
                      size_t offset_i,
                      size_t offset_j);

    // fill the diagonal of the Hso matrix - with the NR excitation energies shifted wrt the lowest one
    void fill_diagonal();

    // read H blocks from the output
        // needs put_block_in()
    void read_H_blocks();

    // construct the SO Hamiltonian (Hso)
        //needs fill_diagonal() & read_H_blocks()
    void build_ham();

    // diagonalizes Hamiltonian
    void diag_ham();

    // construct DO matrix
    void build_dyson_matrix();

    //This function transforms the DO coefficients decomposed over the AO basis to plot the transform DOs
    void Dyson_orbitals();

    // print results to terminal
    void print_results();

    // print NR results to file
    void print_nr_results(bool dyson);

    // print SO results to file
    void print_so_results(bool dyson);

};

#endif // EFF_XAS_H