#include "utils.h"

// check that ouput exists - throw error otherwise
int check_output(std::string output) {
    //Read output
    std::ifstream inFile;
    inFile.open(output);
    //send error if output not found
    if (!inFile) {
        std::cout << "Unable to open output file" << std::endl;
        exit(1); // terminate with error
    } else {
        std::string line;
        while(!(inFile.eof())) {
           std::getline(inFile, line);
           if (line.find("Solving for") != std::string::npos) {
                std::vector<std::string> dummy_line;
                std::istringstream iss(line);
                copy(std::istream_iterator<std::string>(iss), std::istream_iterator<std::string>(), back_inserter(dummy_line));
                if (dummy_line[2] == "CVS-EOMEE-CCSD") {
                    std::cout << "You gave an XAS output" << std::endl << std::endl;
                    inFile.close();
                    return 1;
                } else if (dummy_line[2] == "CVS-EOMIP-CCSD") {
                while (!inFile.eof()) {
                    getline(inFile, line);
                    if (line.find("Dyson orbital calculation") != std::string::npos) {
                        std::cout << "You gave an XPS output" << std::endl << std::endl;
                        inFile.close();
                        return 2;
                    }
                } 
                std::cout << "You gave an IP output" << std::endl << std::endl;
                inFile.close();
                return 3;
            }
           }
        }

        std::cout << "Something went wrong" << std::endl << std::endl;
        inFile.close();
        return 0;



        inFile.close();
        return 0;
    }
}
