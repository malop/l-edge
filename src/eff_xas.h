#ifndef EFF_XAS_H
#define EFF_XAS_H

#ifndef ARMA_DONT_USE_WRAPPER
#define ARMA_DONT_USE_WRAPPER
#endif // ARMA_DONT_USE_WRAPPER

#ifndef ARMA_USE_LAPACK
#define ARMA_USE_LAPACK
#endif // ARMA_USE_LAPACK
#include <armadillo>

#include <iostream> // standard input-output stream (cout/cin)
#include <iomanip> // for setprecison()
#include <fstream> // input/output file stream 
#include <string>
#include <vector>
#include <regex> // regular expressions
#include "state_ee.h"
#include "utils.h"

class eff_xas {
private:
    std::string m_output;                        // output name
    std::vector<State_ee> m_states;              // vector with states
    std::vector<Singlet> m_singlets;              // vector with simglets
    std::vector<Triplet> m_triplets;              // vector with triplets
    size_t m_size = 0;                           // size of matrix
    arma::Mat<std::complex<double>> m_ham;       // hamiltonian
    arma::Mat<std::complex<double>> m_ham_trans; // hamiltonian
    arma::cx_mat m_tdm_x_trans;
    arma::cx_mat m_tdm_y_trans;
    arma::cx_mat m_tdm_z_trans;

    arma::cx_mat m_eigvec;                       // eigenvectors
    arma::vec m_eigval;                          // eigenvalues
public:
    eff_xas(std::string output) : m_output(output) {};

    std::vector<State_ee> get_states() const {return m_states;}
    // functions
    void run();

    // get lowest energy - the energies in the diagonal are shifted wrt the lowest energy
    double get_lowest_energy();

    // read the symmetry of the molecule
    void read_sym(std::string &point_group, 
                  std::vector<std::string> &irreps);

    // find the NR states
    void find_states();


    // put block in matrix 
        // used in: read_H_blocks()
    void put_block_in(const arma::Mat<std::complex<double> > &H_Block, 
                      size_t offset_i, 
                      size_t offset_j);


    // fill the diagonal of the matrix Hso - with the NR excitation energies shifted wrt the lowest one 
        // needs: get_lowest_energy()
    void fill_diagonal();

    // read H blocks from the output
        // needs: put_block_in()
    void read_H_blocks();

    // construct the SO Hamiltonian (Hso)
        // needs: fill_diagonal() & read_H_blocks()
    void build_ham();

    // diagonalize SO Hamiltonian
    void diag_ham();

    // fill TDM1 GS -> ES
    void fill_tdm1(arma::Mat<double>& TDM1, 
                             size_t alpha);

    // fill TDM2 ES -> GS
    void fill_tdm2(arma::Mat<double>& TDM2, 
                             size_t alpha);

    // contruct transition dipole moment matrices (TDM) between ground state (GS) and excited states (ES)
        // needs: fill_tdm1() & fill_tdm2()
    void tdm();

    // print NR results to file: name+_NR.txt 
    void print_nr_results(); 

    // print SO results to file: name+_SO.txt 
    void print_so_results();
};
#endif // EFF_XAS_H