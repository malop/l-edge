#ifndef UTILS_H
#define UTILS_H

#include <iostream> // standard input-output stream (cout/cin)
#include <iomanip>  // for setprecison()
#include <fstream>  // input/output file stream

#include <string> // pretty self-explanatory
#include <vector>

#include <regex> // regular expressions


// global constants
const double ev2cm = 8065.5;
const double au2ev = 27.2114;

// check that ouput exists - throw error otherwise
int check_output(std::string output);


#endif // UTILS_H