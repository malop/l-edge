set(Headers
    utils.h
    eff_xps.h
    eff_xas.h
    state.h
    state_ip.h
    state_ee.h
)

set(Sources
    main.cpp
    utils.cpp
    eff_xps.cpp
    eff_xas.cpp
)
add_executable(l-edge ${Sources})
target_include_directories(l-edge PUBLIC "${PROJECT_DIR}")
target_link_libraries(l-edge armadillo)

install(TARGETS l-edge DESTINATION lib)
install(FILES ${Headers} DESTINATION include)

install(TARGETS l-edge DESTINATION bin)