# Overview 
The l-edge program calculates the spin-orbit corrected energies for a given Q-Chem output, as described in [https://doi.org/10.1021/acs.jpclett.0c02027](https://doi.org/10.1021/acs.jpclett.0c02027).

# Prerequisites
1. [cmake](https://cmake.org) version 3.11
2. C++ compiler
3. [BLAS](http://www.netlib.org/blas/) and [LAPACK](http://www.netlib.org/lapack/) libraries

# Get started
1. Clone repository:
    ~~~~
    git clone https://gitlab.com/malop/l-edge.git
    ~~~~
2. Create build directory:
    ~~~~
    cd l-edge
    mkdir build
    ~~~~
3. Run cmake
    ~~~~
    cd build
    cmake ..
    ~~~~
4. Build
    ~~~~
    make
    ~~~~
5. Install
    ~~~~
    make install
    ~~~~
It will install by default in `/usr/local`, which can be changed by running cmake with the flag: `-DCMAKE_INSTALL_PREFIX=<your-preferred-path>` 

If you skip this step, the executable file will be in `l-edge/build/src/l-edge`

6. Test it
    ~~~~
    ctest
    ~~~~
7. Give it a try
    ~~~~
    cd ../examples/xas
    l-edge h2s_xas_soc_vdz.out
    ~~~~
This will create two text files with the non-relativistic (NR) results and the spin-orbit (SO) corrected ones. These files contain two columns: the first corresponds to the excitation energies (eV) and the second to the oscillator strength.

8. In the directory `scripts`, you will find some example python scripts to plot the spectra
    ~~~~
    cd scripts
    python plot_xas.py ../examples/xas/h2s_xas_soc_vdz
    ~~~~

# Q-Chem 
This program only works with a Q-Chem output. This section describes how to set up the corresponding input. For more information about how to run Q-Chem, see [https://manual.q-chem.com/](https://manual.q-chem.com/).

## X-Ray Absorption Spectra (XAS)
To calculate the L-edge XPS, you will need to compute singlet (`cvs_ee_singlets`) and triplet (`cvs_sf_states`) states, and activate the calculation of the spin-orbit coupling between them (`calc_soc = 1`), as well as state and transition properties. The specific state and transition properties to be calculated need to be specified in the section `$trans_prop`. An example is given below.


    $comment
    Example
    H2S (CCSD(T)/cc-pCVQZ optimized geometry)
    S L-edge XAS SOC fc-CVS-EOM- EE+SF / vdz
    $end

    $molecule
    0 1
    S
    H     1  1.335272
    H     1  1.335272    2 92.290
    $end

    $rem
    jobtype = sp
    method = ccsd
    basis = cc-pvdz
    n_frozen_core = fc
    cvs_ee_singlets = [2,2,2,2]
    cvs_sf_states = [2,2,2,2]
    calc_soc = 1
    cc_eom_prop = true
    cc_ref_prop = 1
    cc_trans_prop = 1
    $end

    $trans_prop
    state_list
    ref
    cvs_ee_singlets 1 1
    cvs_ee_singlets 1 2
    cvs_ee_singlets 2 1
    cvs_ee_singlets 2 2
    cvs_ee_singlets 3 1
    cvs_ee_singlets 3 2
    cvs_ee_singlets 4 1
    cvs_ee_singlets 4 2
    cvs_sf_states 1 1
    cvs_sf_states 1 2
    cvs_sf_states 2 1
    cvs_sf_states 2 2
    cvs_sf_states 3 1
    cvs_sf_states 3 2
    cvs_sf_states 4 1
    cvs_sf_states 4 2

    end_list

    calc dipole soc opdm_norm
    $end

## X-Ray Photoelectron Spectroscopy (XPS)
To calculate the L-edge XAS, you will need to compute the desired ionized states (`cvs_ip_states`) and activate the calculation of the spin-orbit coupling between them (`calc_soc = 1`), as well as the transition properties (`cc_trans_prop = true`) and dyson orbitals (`cc_do_dyson = true`). 
The specific state and transition properties to be calculated need to be specified in the section `$trans_prop`. An example is given below.


    $comment
    Example
    H2S (CCSD(T)/cc-pCVQZ optimized geometry)
    S L-edge XPS SOC fc-CVS-EOM-IP / vdz
    $end

    $molecule
    0 1
    S
    H     1  1.335272
    H     1  1.335272    2 92.290
    $end

    $rem
    jobtype = sp
    method = ccsd
    basis = cc-pVDZ
    print_general_basis = true
    n_frozen_core = fc
    cvs_ip_states = [3]
    calc_soc = 1
    cc_trans_prop = true
    cc_do_dyson = true
    molden_format = true
    $end

    $trans_prop
    state_list
    ref
    cvs_ip_states 1 1

    end_list

    calc dyson
    state_list
    ref
    cvs_ip_states 1 2

    end_list

    calc dyson

    state_list
    ref
    cvs_ip_states 1 3

    end_list

    calc dyson

    state_list
    cvs_ip_states 1 1
    cvs_ip_states 1 2
    cvs_ip_states 1 3

    end_list

    calc soc
    $end

If you are only interested in the ionization energies, you don't need to calculate the dyson orbitals. In this case, you only need to specify the ionized states you want (`cvs_ip_states`) and activate the calculation of SOC (`calc_soc = 1`) and transition properties between ionized states (`cc_trans_prop = 2`). Yet an example is given hereafter.


    $comment
    H2S (CCSD(T)/cc-pCVQZ optimized geometry)
    S L-edge XAS SOC fc-CVS-EOMIP / cc-pVDZ
    $end

    $molecule
    0 1
    S
    H     1  1.335272
    H     1  1.335272    2 92.290
    $end

    $rem
    method = ccsd
    basis = cc-pvdz
    n_frozen_core = fc
    cvs_ip_states = [1,0,1,1]
    calc_soc = 1
    cc_trans_prop = 2
    $end


# Disclaimer
Use at your own risk


