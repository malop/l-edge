
#!/usr/env python
from sys import argv, stdout
import glob
import os
import subprocess
import sys
import numpy as np
import matplotlib.pyplot as plt
from pylab import *
from matplotlib.ticker import FormatStrFormatter

'''
This python script takes a soc-cvs-eomip output as argument
and plots the spectra before and after applying the spin-orbit correction. 

It needs the .txt files written by the program l-edge
So, after running the l-edge program, run:
python plot_xps.py <qchem_xps_output_name>

for example, try:
python plot_xps.py ../examples/xps/h2s_xps_soc_vdz
'''

# debug variable
# Set as True for debugging purposes
debug = False

# plotting variables
# set to True to set_parameters manually
set_params  = False
def set_parameters(xmin,xmax,ymin,ymax,xshift):
    #H2S
    xshift = 0.0
    xmin=168
    xmax=172
    return xmin, xmax, ymin, ymax, xshift


# Lorentz convolution function
def lorentz(omega, exci, sigma,forza):
    grecopi=3.14159265358979323846264338327950288
    return (sigma*forza)/(grecopi*((omega-exci)**2+sigma**2))

# Gaussian convolution function: g(x) = 1/(sigma*sqrt(2*pi)) * exp(-0.5*(x-exci)/sigma)
def gaussian(omega, exci, sigma, weight):
    # omega: photon energy
    # exci: excitation energy
    # sigma: standard deviation
    # weight: oscillator strength
    grecopi=3.14159265358979323846264338327950288 # I think we need more decimals..
    fact=1.0/(sigma*sqrt(2*grecopi))
    func=fact*exp(-(omega-exci)**2/(2*sigma**2))
    return weight*func

# Convolution function
# Reads the results from a .txt file and prints the convoluted spectra to .dat file
def convolute(name):
    print("convolute()")
    # Open .txt file to read the raw data
    data = np.genfromtxt(name+".txt")
    exci = data[:,0] # excitation energy in eV
    f = data[:,1] # oscillator strength
    #
    # Open .dat file to write the convoluted data
    file = open(name+".dat","w")
    #
    # Spectral parameters
    sigma = 0.1 # standard deviation in eV (sigma=gamma/2) 
    step = 0.01
    omega = np.arange(min(exci)-5,max(exci)+5,step) #Photon energy range 
    n=len(exci)
    spec = [] # spectral intensity
    for i in range(0, len(omega), 1):
           spec.append(0.0)
    for i in range(0, len(exci), 1):
        for n in range(0, len(omega), 1):
            spec[n]=spec[n]+gaussian(omega[n],exci[i],sigma,f[i])
    for i in range(len(omega)):
        file.write("{0:.4f}".format(omega[i]))
        file.write("    ")
        file.write("{0:.6f}".format(spec[i]))
        file.write("\n")

# plot functions
def plot_xps_nr_so(name):
    print("plot()")
    # NR
    # Read data from .txt file with excitation energies and oscillator strengths (sticks)
    data1 = np.genfromtxt(name+"_NR.txt")
    stcksx1 = data1[:,0] 
    stcksy1 = data1[:,1]
    # Read .dat file withconvoluted data (spectrum)
    data1 = np.genfromtxt(name+"_NR.dat")
    x1 = data1[:,0] 
    y1 = data1[:,1]
    #
    # SO
    data2 = np.genfromtxt(name+"_SO.txt")
    stcksx2 = data2[:,0] 
    stcksy2 = data2[:,1]
    # Read .dat file withconvoluted data (spectrum)
    data2 = np.genfromtxt(name+"_SO.dat")
    x2 = data2[:,0] 
    y2 = data2[:,1]
    # for math text 
    params = {'mathtext.default': 'regular' }          
    plt.rcParams.update(params)
    # plot options:
    #General
    xshift = 0
    xmin = x1.min()
    xmax = x1.max()
    ymin = y1.min()
    ymax = y2.max()*1.1

    if (set_params):
        xmin, xmax, ymin, ymax, xshift = set_parameters(xmin,xmax,ymin,ymax,xshift)

    plt.xlim(xmin,xmax)
    plt.ylim(ymin,ymax)
    yscale = ymax*0.7


    plt.xlabel('Photon energy (eV)')
    plt.ylabel('Intensity (arb. units)')
    yip = (y1.max())

    plt.plot(x1+xshift, y1, 'red', label='NR', linewidth=2)#, linestyle='--')
    plt.plot(x2+xshift, y2, 'b', label='SO', linewidth=2)#, linestyle='--')
    n=len(stcksx1)
    for i in range(n):
        plt.plot([stcksx1[i]+xshift,stcksx1[i]+xshift],[0,stcksy1[i]],'r-',linewidth=1.0)
    n=len(stcksx2)
    for i in range(n):
        plt.plot([stcksx2[i]+xshift,stcksx2[i]+xshift],[0,stcksy2[i]],'b-',linewidth=1.0)
    plt.legend(loc='best',fontsize='small')
    #

    plt.savefig(name+"_xps_nr_so.pdf")
    plt.show()

def plot_xps_nr_so_exp(name):
    print("plot()")
    mol = name[:name.index("_")]
    xshift = 0.0#
    # Read data from .txt file with excitation energies and oscillator strengths (sticks)
    data1 = np.genfromtxt(name+"_NR.txt")
    stcksx1 = data1[:,0] 
    stcksy1 = data1[:,1]
    # Read .dat file withconvoluted data (spectrum)
    data1 = np.genfromtxt(name+"_NR.dat")
    x1 = data1[:,0] 
    y1 = data1[:,1]
    #
    data2 = np.genfromtxt(name+"_SO.txt")
    stcksx2 = data2[:,0] 
    stcksy2 = data2[:,1]
    # Read .dat file withconvoluted data (spectrum)
    data2 = np.genfromtxt(name+"_SO.dat")
    x2 = data2[:,0] 
    y2 = data2[:,1]
    #
    # Exp
    data0 = np.genfromtxt(mol+"_xps_exp.csv")
    x0 = data0[:,0] 
    y0 = data0[:,1]
    #ip0 = [107.0952380952381, 107.69047619047619]

    #f = interp1d(x0, y0, kind='slinear')
    #x_exp = np.linspace(x0.min(),x0.max(),10000)
    #y_exp = f(x_exp)

    # for math text 
    params = {'mathtext.default': 'regular' }          
    plt.rcParams.update(params)
    # Two plots
    f, (ax1, ax2) = plt.subplots(2, sharex=True, sharey=True, figsize = (5,5))
    # plot options:

    plt.title('')
    plt.xlabel('Ionization energy (eV)')
    
    #General
    xshift = 0
    xmin = x1.min()
    xmax = x1.max()
    ymin = y1.min()
    ymax = y1.max()

    if (set_params):
        xmin, xmax, ymin, ymax, xshift = set_parameters(xmin,xmax,ymin,ymax,xshift)

    plt.xlim(xmin,xmax)
    plt.ylim(ymin,ymax*1.1)

    yscale = ymax*0.7

    plt.yticks([])
    plt.xticks(np.arange(170, 173, 0.5))
    #plt.ylim(0.0,y2.max()*1.2)

    f.text(0.04, 0.5, 'Intensity (arb. units)', va='center', rotation='vertical')
    ax2.plot(x0, y0*yscale, 'k', label='Experiment', linewidth=1)#, linestyle='--')
    
    ax1.plot(x0.min(), 0.0, 'white', label='Theory ($\Delta$$\it{x} = %.2f$ eV)' %xshift , linewidth=2)#, linestyle='--')
    
    ax1.plot(x1+xshift, y1, 'red', label='NR', linewidth=2, linestyle='--')
    ax1.plot(x2+xshift, y2, 'b', label='SO', linewidth=2)#, linestyle='--')
    n=len(stcksx1)
    for i in range(n):
        ax1.plot([stcksx1[i]+xshift,stcksx1[i]+xshift],[0,stcksy1[i]],'r-',linewidth=1.0)
    n=len(stcksx2)
    for i in range(n):
        ax1.plot([stcksx2[i]+xshift,stcksx2[i]+xshift],[0,stcksy2[i]],'b-',linewidth=1.0)
    ax1.legend(loc='best',fontsize='small')
    #
    ax1.legend(loc='best',fontsize='small')
    ax2.legend(loc='best',fontsize='small')
    #
    f.subplots_adjust(hspace=0)
    plt.setp([a.get_xticklabels() for a in f.axes[:-1]], visible=False)

    plt.savefig(name+"_nr_so_exp.pdf")
    plt.show()

# main function
if __name__ == '__main__':
    name = argv[1]
    file_NR = name+"_NR"
    convolute(file_NR)
    file_SO = name+"_SO"
    convolute(file_SO)
    plot_xps_nr_so(name)
    #plot_xps_nr_so_exp(name)
