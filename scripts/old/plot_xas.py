#!/usr/env python
from sys import argv, stdout
import glob
import os
import subprocess
import sys
import numpy as np
import matplotlib.pyplot as plt
from pylab import *
from matplotlib.ticker import FormatStrFormatter


'''
Marta December 2020

This python script takes a Q-Chem output as argument. 
For it to work, it needs the files:
 - xas.exe
 - ip.exe
 - xas output
 - ip output with same name as the xas output replacing xas by xps

The program
1. Runs the executable files ee.exe located in /l-edge/scripts/xas/ 
2. Runs the executable files ip.exe located in /l-edge/scripts/xas/ 
3. Reads the results and prints the corresponding spectrum to a .dat file
4. Plots the NR & SO spectra

'''


# debug variable
# Set as True for debugging purposes
debug = False

# plotting variables
# set to True to set_parameters manually
set_params = False
def set_parameters(xmin,xmax,ymin,ymax,xshift):
    #H2S
    ymin = 0
    ymax = 0.06
    xmin = 162
    xmax = 172
    xshift = 0.6

    return xmin, xmax, ymin, ymax, xshift

# runs ./xas.exe 
def run_eff_ee(out):
    command = "../../scripts/xas/xas.exe "+out
    print(command)
    os.system(command)

# runs ./eff_ip.exe 
def run_eff_ip(out):
    out_ip = out.replace("xas","xps")
    command = "../../scripts/xas/ip.exe "+out_ip
    print(command)
    os.system(command)

# Lorentz convolution function
def lorentz(omega, exci, sigma,forza):
    grecopi=3.14159265358979323846264338327950288
    return (sigma*forza)/(grecopi*((omega-exci)**2+sigma**2))

# Gaussian convolution function: g(x) = 1/(sigma*sqrt(2*pi)) * exp(-0.5*(x-exci)/sigma)
def gaussian(omega, exci, sigma, weight):
    # omega: photon energy
    # exci: excitation energy
    # sigma: standar deviation
    # weight: oscillator strength
    grecopi=3.14159265358979323846264338327950288 # I think we need more decimals..
    fact=1.0/(sigma*sqrt(2*grecopi))
    func=fact*exp(-(omega-exci)**2/(2*sigma**2))
    return weight*func

# Convolution function
# Reads the results from a .txt file and prints the convoluted spectra to .dat file
def convolute(name):
    print("convolute()")
    # Open .txt file to read the raw data
    data = np.genfromtxt(name+".txt")
    exci = data[:,0] # excitation energy in eV
    f = data[:,1] # oscillator strength
    #
    # Open .dat file to write the convoluted data
    file = open(name+".dat","w")
    #
    # Spectral parameters
    sigma = 0.15 # standard deviation in eV (sigma=gamma/2) 
    step = 0.01
    omega = np.arange(min(exci)-5,max(exci)+5,step) #Photon energy range 
    n=len(exci)
    spec = [] # spectral intensity
    for i in range(0, len(omega), 1):
           spec.append(0.0)
    for i in range(0, len(exci), 1):
        for n in range(0, len(omega), 1):
            spec[n]=spec[n]+gaussian(omega[n],exci[i],sigma,f[i])
    for i in range(len(omega)):
        file.write("{0:.4f}".format(omega[i]))
        file.write("    ")
        file.write("{0:.6f}".format(spec[i]))
        file.write("\n")

#plot function
def plot_nr_so(name):
    print("plot()")
    name_ip = name.replace("xas","xps")

    # NR
    # read data from .txt file with excitation energies and oscillator strengths (sticks)
    data1 = np.genfromtxt(name+"_NR.txt")
    stcksx1 = data1[:,0] 
    stcksy1 = data1[:,1]
    # read .dat file with convoluted data (spectrum)
    data1 = np.genfromtxt(name+"_NR.dat")
    x1 = data1[:,0] 
    y1 = data1[:,1]
    #
    # read IP
    data1 = np.genfromtxt(name_ip+"_NR.txt")
    ip1 = data1[:]
    #
    # SO
    # read .txt file with excitation energies and oscillator strengths (sticks)
    data2 = np.genfromtxt(name+"_SO.txt")
    stcksx2 = data2[:,0] 
    stcksy2 = data2[:,1]
    # read .dat file with convoluted data (spectrum)
    data2 = np.genfromtxt(name+"_SO.dat")
    x2 = data2[:,0] 
    y2 = data2[:,1]
    # read IP
    data2 = np.genfromtxt(name_ip+"_SO.txt")
    ip2 = data2[:]
    #
    # for math text 
    params = {'mathtext.default': 'regular' }          
    plt.rcParams.update(params)

    # plot options:
    plt.title('')
    plt.xlabel('Photon energy (eV)')
    plt.ylabel('Intensity (arb. units)')

    #General
    xshift = 0
    xmin = x1.min()
    xmax = x1.max()
    ymin = y1.min()
    ymax = y1.max()*1.1

    if (set_params):
        xmin, xmax, ymin, ymax, xshift = set_parameters(xmin,xmax,ymin,ymax,xshift)

    plt.xlim(xmin,xmax)
    plt.ylim(ymin,ymax)

    plt.plot(x1+xshift, y1, 'red', label='NR', linewidth=2, linestyle='--')
    plt.plot(x2+xshift, y2, 'b', label='SO', linewidth=2)#, linestyle='--')
    
    plt.plot([ip1+xshift,ip1+xshift],[0,ymax],'r',linewidth=1.0, linestyle='dotted')
    plt.plot([ip2+xshift,ip2+xshift],[0,ymax],'b',linewidth=1.0, linestyle='dotted')
    
    n=len(stcksx1)
    for i in range(n):
        plt.plot([stcksx1[i]+xshift,stcksx1[i]+xshift],[0,stcksy1[i]],'r-',linewidth=1.0)
    
    n=len(stcksx2)
    for i in range(n):
        plt.plot([stcksx2[i]+xshift,stcksx2[i]+xshift],[0,stcksy2[i]],'b-',linewidth=1.0)
    
    plt.legend(loc='upper left',fontsize='small')
    #
    plt.savefig(name+"_nr_so.pdf")
    plt.show()

# main function
if __name__ == '__main__':
    with open(argv[1],'r') as fd:
        out = argv[1]
        name = out[:-4]
        run_eff_ee(out)
        run_eff_ip(out)
        convolute(name+"_NR")
        convolute(name+"_SO")
        plot_nr_so(name)
